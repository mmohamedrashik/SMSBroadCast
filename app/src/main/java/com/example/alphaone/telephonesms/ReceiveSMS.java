package com.example.alphaone.telephonesms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 * Created by ALPHA ONE on 9/6/2015.
 */
public class ReceiveSMS extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        SmsMessage[] smsMessages = null;
        String str="";
        if(bundle!=null)
        {
            Object[] pdus = (Object[])bundle.get("pdus");
            smsMessages = new SmsMessage[pdus.length];
            for (int x=0;x<smsMessages.length;x++)
            {
                smsMessages[x]=SmsMessage.createFromPdu((byte[])pdus[x]);
                str += "SMS HAS BEEN RECICED"+smsMessages[x].getOriginatingAddress();
                str += "BODY"+smsMessages[x].getMessageBody().toString();
            }
            Toast.makeText(context,str,Toast.LENGTH_LONG).show();
        }
    }
}
